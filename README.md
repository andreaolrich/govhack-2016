### Installation

Install nodemon globally:

    npm install -g nodemon

Install and run the app:

    npm install
    nodemon app.js

No need to restart the server to see changes, just refresh the app. 
