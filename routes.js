/*
* Application logic
*
* This file contains a set of functions describing
* the behaviour of the app. For each route with a
* specified request method e.g. GET and POST etc.
*
* The corresponding view is rendered using the
* information served by the database logic functions.
*/

//////////////
//  Setup   //
//////////////

// Require and create the router
var Router = require('koa-router');
var router = Router();

////////////////////
//  User routes   //
////////////////////

// Index route
router.get('/', function* (next) {
    yield this.render('index.html');
});
// Suburbs routes
router.get('/api/suburb', function* (next) {
    this.body = yield db.query('select * from suburb');
});
router.get('/api/suburb/:id', function* (next) {
    this.body = yield db.query("select * from suburb where suburb_id = '${id#}'", {id: this.params.id});
});
// Schools routes
router.get('/api/school', function* (next) {
    // gets you the number of schools per postcode in descending order of number of schools
    this.body = yield db.query("select postcode, count(distinct school_id) as num_schools from schools group by postcode order by num_schools desc")
});
router.get('/api/school/:postcode', function* (next) {
    // gets you details of all the schools for a given postcode
    this.body = yield db.query("select * from schools where postcode = '${postcode#}'", {postcode: this.params.postcode});
});
// Property prices routers
// has a query property ?type=rent or type=buy
// defaults to buy
// also takes a min_price and max_price
router.get('/api/property', function* (next) {
    this.min = this.query.min_price || 0;
    this.max = this.query.max_price || 2215001; // hard coded to the largest price in the db currently plus 1
    this.table = (this.query.type === 'rent') ? 'property_rental_price' : 'property_sale_price';
    this.body = yield db.query("select * from ${table#} where price > ${min#} and price < ${max#}", {table: this.table, min: this.min, max: this.max});
});
// Crime rate routes
router.get('/api/crime', function* (next) {
    // gets the average crime rate for every postcode
    this.body = yield db.query("select round(avg(c.average_rate)) as average_rate, l.postcode from lga_to_postcode l, crimerates c where lower(l.lga) = lower(c.lga) group by l.postcode");
});
// params:
// crime = true
// schools = true
// level = primary
// level = secondary
// price = true
// max = X
// min = Y
router.get('/api/search', function* (next) {
    this.min = 0;
    this.max = 2215000;
    // Assume if price = true that there's a min/max.
    if (this.query.price) {
        this.min = this.query.min;
        this.max = this.query.max;
    }
    // both crime and schools
    if (this.query.schools && this.query.crime) {
        if (this.query.level) {
            if (this.query.level.length === 2) {
                this.body = yield db.query("SELECT y.postcode, CASE WHEN y.num_schools BETWEEN 1 and 5 THEN 0.25 WHEN y.num_schools BETWEEN 6 and 10 THEN 0.5 WHEN y.num_schools BETWEEN 11 and 15 THEN 0.75 WHEN y.num_schools > 16 THEN 1 ELSE 0 END as school_score, x.crime_score from (select postcode, count(distinct school_id) as num_schools from schools where level in ('Primary School', 'Secondary School') group by postcode) y, (select l.postcode, CASE WHEN round(avg(c.average_rate)) is null THEN 0.5 WHEN round(avg(c.average_rate))=0 THEN 1 ELSE round((4 - round(avg(c.average_rate)))/4, 2) END as crime_score from lga_to_postcode l, crimerates c where lower(l.lga) = lower(c.lga) group by l.postcode) x, property_sale_price p where x.postcode = y.postcode and y.postcode = p.postcode and p.price between ${min#} and ${max#}", {min: this.min, max: this.max});
            } else if (this.query.level === 'primary') {
                this.body = yield db.query("SELECT y.postcode, CASE WHEN y.num_schools BETWEEN 1 and 5 THEN 0.25 WHEN y.num_schools BETWEEN 6 and 10 THEN 0.5 WHEN y.num_schools BETWEEN 11 and 15 THEN 0.75 WHEN y.num_schools > 16 THEN 1 ELSE 0 END as school_score, x.crime_score from (select postcode, count(distinct school_id) as num_schools from schools where level in ('Primary School') group by postcode) y, (select l.postcode, CASE WHEN round(avg(c.average_rate)) is null THEN 0.5 WHEN round(avg(c.average_rate))=0 THEN 1 ELSE round((4 - round(avg(c.average_rate)))/4, 2) END as crime_score from lga_to_postcode l, crimerates c where lower(l.lga) = lower(c.lga) group by l.postcode) x, property_sale_price p where x.postcode = y.postcode and y.postcode = p.postcode and p.price between ${min#} and ${max#}", {min: this.min, max: this.max});
            } else if (this.query.level === 'secondary') {
                this.body = yield db.query("SELECT y.postcode, CASE WHEN y.num_schools BETWEEN 1 and 5 THEN 0.25 WHEN y.num_schools BETWEEN 6 and 10 THEN 0.5 WHEN y.num_schools BETWEEN 11 and 15 THEN 0.75 WHEN y.num_schools > 16 THEN 1 ELSE 0 END as school_score, x.crime_score from (select postcode, count(distinct school_id) as num_schools from schools where level in ('Secondary School') group by postcode) y, (select l.postcode, CASE WHEN round(avg(c.average_rate)) is null THEN 0.5 WHEN round(avg(c.average_rate))=0 THEN 1 ELSE round((4 - round(avg(c.average_rate)))/4, 2) END as crime_score from lga_to_postcode l, crimerates c where lower(l.lga) = lower(c.lga) group by l.postcode) x, property_sale_price p where x.postcode = y.postcode and y.postcode = p.postcode and p.price between ${min#} and ${max#}", {min: this.min, max: this.max});
            }
        } else {
            // no level of education query given.
            this.body = yield db.query("SELECT y.postcode, CASE WHEN y.num_schools BETWEEN 1 and 5 THEN 0.25 WHEN y.num_schools BETWEEN 6 and 10 THEN 0.5 WHEN y.num_schools BETWEEN 11 and 15 THEN 0.75 WHEN y.num_schools > 16 THEN 1 ELSE 0 END as school_score, x.crime_score from (select postcode, count(distinct school_id) as num_schools from schools group by postcode) y, (select l.postcode, CASE WHEN round(avg(c.average_rate)) is null THEN 0.5 WHEN round(avg(c.average_rate))=0 THEN 1 ELSE round((4 - round(avg(c.average_rate)))/4, 2) END as crime_score from lga_to_postcode l, crimerates c where lower(l.lga) = lower(c.lga) group by l.postcode) x, property_sale_price p where x.postcode = y.postcode and y.postcode = p.postcode and p.price between ${min#} and ${max#}", {min: this.min, max: this.max});
        }
    } else if (this.query.schools) {
        // just care about schools
        if (this.query.level) {
            if (this.query.level.length === 2) {
                this.body = yield db.query("SELECT y.postcode, CASE WHEN y.num_schools BETWEEN 1 and 5 THEN 0.25 WHEN y.num_schools BETWEEN 6 and 10 THEN 0.5 WHEN y.num_schools BETWEEN 11 and 15 THEN 0.75 WHEN y.num_schools > 16 THEN 1 ELSE 0 END as school_score from (select postcode, count(distinct school_id) as num_schools from schools where level in ('Primary School', 'Secondary School') group by postcode) y order by school_score desc");
            } else if (this.query.level === 'primary') {
                this.body = yield db.query("SELECT y.postcode, CASE WHEN y.num_schools BETWEEN 1 and 5 THEN 0.25 WHEN y.num_schools BETWEEN 6 and 10 THEN 0.5 WHEN y.num_schools BETWEEN 11 and 15 THEN 0.75 WHEN y.num_schools > 16 THEN 1 ELSE 0 END as school_score from (select postcode, count(distinct school_id) as num_schools from schools where level in ('Primary School') group by postcode) y order by school_score desc");
            } else if (this.query.level === 'secondary') {
                this.body = yield db.query("SELECT y.postcode, CASE WHEN y.num_schools BETWEEN 1 and 5 THEN 0.25 WHEN y.num_schools BETWEEN 6 and 10 THEN 0.5 WHEN y.num_schools BETWEEN 11 and 15 THEN 0.75 WHEN y.num_schools > 16 THEN 1 ELSE 0 END as school_score from (select postcode, count(distinct school_id) as num_schools from schools where level in ('Secondary School') group by postcode) y order by school_score desc");
            }
        } else {
            // no level of education query given.
            this.body = yield db.query("SELECT y.postcode, CASE WHEN y.num_schools BETWEEN 1 and 5 THEN 0.25 WHEN y.num_schools BETWEEN 6 and 10 THEN 0.5 WHEN y.num_schools BETWEEN 11 and 15 THEN 0.75 WHEN y.num_schools > 16 THEN 1 ELSE 0 END as school_score from (select postcode, count(distinct school_id) as num_schools from schools group by postcode) y order by school_score desc");
        }
    } else if (this.query.crime) {
        // just care about crime
        // scores of null, 0, 1, 2, 3, 4 become 0.5, 1, 0.75, 0.5, 0.25, 0 respectively
        this.body = yield db.query("select l.postcode, CASE WHEN round(avg(c.average_rate)) is null THEN 0.5 WHEN round(avg(c.average_rate))=0 THEN 1 ELSE round((4 - round(avg(c.average_rate)))/4, 2) END as crime_score from lga_to_postcode l, crimerates c where lower(l.lga) = lower(c.lga) group by l.postcode order by crime_score desc")
    }
});

/////////////////////////
//  Export the router  //
/////////////////////////

// Export the router
module.exports = router;
