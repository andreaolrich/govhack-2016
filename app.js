var koa = require('koa');
var body = require('koa-body');
var views = require('koa-views');
var serve = require('koa-static');
var logger = require('koa-logger');
var postgres = require('pg-promise');

var app = koa();

// Database configuration
var config = require('./config.json');

// Connect to the database
global.db = postgres()(config.postgres);

// Setup error handler
app.use(function* (next) {
  try {
    yield next;
  } catch (err) {
    // Caught an error that was raised in one of the middlewares.
    // Respond with information about the error.
    this.body = err.message;
    this.status = err.status || 500;
    console.error('Error:', err);
  }
});

// Setup template engine
app.use(views(__dirname + '/client',
   { default: 'html' })
);

// Setup logger middleware
app.use(logger());

// Setup body parser middleware
app.use(body());

// Setup server routes
var router = require('./routes');
app.use(router.routes());

// Setup static assets
app.use(serve('client'));

// Serve angular
app.use(serve('node_modules/'));

// Start the web server
var port = process.env.PORT || 3000;
var server = app.listen(port, function() {
  console.log('Koa listening on port', port);
});
