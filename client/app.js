var homeApp = angular.module('homeApp', ['ngSanitize', 'ngMaterial', 'rzModule']);

homeApp.controller("MainController", function MainController($scope, $mdSidenav, $sanitize) {
    var ctrl = this;
    ctrl.destination = "Rosebery, NSW 2018";
    ctrl.updated=false;
    $scope.results = {};
    var Origins = ["HAYMARKET, NSW 2000","THE ROCKS, NSW 2000","DAWES POINT, NSW 2000","MILLERS POINT, NSW 2000","SYDNEY, NSW 2000","BARANGAROO, NSW 2000"];
    var TransitOptions = {
        modes: ['BUS', 'TRAIN'],
    };
    // Each time the destination is updated, it fires off a distance matrix request.
    this.search = function(request) {
        ctrl.updated = true;
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: Origins,
                destinations: [this.destination],
                travelMode: 'TRANSIT',
                transitOptions: TransitOptions
            }, function(response, status) {
                ctrl.results = response;
            });
        var request = {
            origin: Origins[0],
            destination: this.destination,
            travelMode: 'TRANSIT'
        };
        var directionsService = new google.maps.DirectionsService();
        directionsService.route(request, function(response, status) {
            console.log(response);
            ctrl.directions = response;
        });
    }
});

homeApp.component('sidebar', {
    bindings: {
        destination: '<',
        onUpdate: '&'
    },
    templateUrl: 'sidebar/sidebar.html',
    controller: SidebarController
});

homeApp.component('map', {
    bindings: {
        destination: '='
    },
    templateUrl: 'map/map.html',
    controller: MapController
});

homeApp.component('results', {
    bindings: {
        res: '=',
        dir: '='
    },
    templateUrl: 'results/results.html',
    controller: ResultsController
});

function SidebarController($scope) {
    this.update = function(form) {
        this.onUpdate({request: form});
    };
    var input = document.getElementById('searchTextField');
    var options = {
        componentRestrictions: {country: 'au'}
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);
    $scope.form = {
        price: {
            min: 0,
            max: 2215000,
            options: {
                ceil: 2215000,
                hideLimitLabels: true,
                hidePointerLabels: true,
                floor: 0,
                step: 50000,
                translate: function(value) {
                    return '$' + value;
                }
            }
        }
    };
    this.reset = function() {
        $scope.form = {};
    };
}

function MapController($window) {
    $window.map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 8
    });

    new google.maps.Marker({
        position: {lat: -33.808631, lng: 151.184019},
        map: $window.map
    });

    new google.maps.Marker({
        position: {lat: -33.773954, lng: 151.018027},
        map: $window.map
    });

    new google.maps.Marker({
        position: {lat: -33.807991, lng: 151.111194},
        map: $window.map
    });
}

function ResultsController($scope) {
}
